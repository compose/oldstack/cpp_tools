# timers : a collection of helpful timers

:construction: New timers will be added !

# Simple timer example

```cpp
#include <iostream>
#include <cpp_tools/timers/simple_timer.hpp>

...
auto main() -> int
{
    ...

    // default is millisecond
    cpp_tools::timers::timer time{};
    // or cpp_tools::timers::timer<std::chrono::microseconds> time{}; 
    // for timings in milliseconds etc ...

    time.tic();
    // some stuff to time...
    
    // directly : std::cout << " time elapsed : " << time.tac_and_elapsed() << "ms\n";
    // or :

    time.tac();
    std::cout << " time elapsed : " << time.elapsed() << "ms\n";
    ...
}
```

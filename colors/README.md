# colors : simple colors for you terminal

`colors` is a single header to put color in your terminal.

# Usage

```cpp
#include <iostream>
#include <cpp_tools/colors/colorized.hpp>
...
std::cout << cpp_tools::colors::blue << "some blue" << cpp_tools::colors::reset << '\n';
...
```
# Available colors


`black`
`red`
`green`
`yellow`
`blue`
`magenta`
`cyan`
`white`
`on_black`
`on_red`
`on_green`
`on_yellow`
`on_blue`
`on_magenta`
`on_cyan`
`on_white`
`reset`
`bold`
`faint`
`italic`
`underline`
`underline_off`
`slow_blink`
`rapid_blink`
`blink_off`
`reverse`
`overlined`

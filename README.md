# cpp_tools : small cpp tools to reuse in your projects

`cpp_tools` is a collection of headers organised as modules.
The fisrt effort here, is to use this repository as a submodule in your lib/app.

For now, the following modules are available :
 - cl_parser : a command line parser to pass command line arguments to your binaries.
 - parallel_manager : a utility class to manage mpi and openmp.
 - colors : a terminal coloring tool.
 - timers : a collection of timers for benchmarking/tracing your lib/app.

## Using this repo as a git submodule

One you have added this repo as a submodule to your own repo, `cpp_tools` provides
a simple way to initialyse the tools in your cmake.

First you need to update your cmake module path with the path to root of the submodule :

```cmake
list(APPEND CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/path/to/cpp_tools/)
```
Then you need to set the `CPP_TOOLS_ROOT` variable with the path to the root of
the submodule to make `cpp_tools` aware of its own location :

```cmake
set(CPP_TOOLS_ROOT ${CMAKE_CURRENT_SOURCE_DIR}/path/to/cpp_tools/)
```
Now you need to specify which modules you want to use :

```cmake
set(CPP_TOOLS_USE_CL_PARSER ON)
set(CPP_TOOLS_USE_COLORS ON)
set(CPP_TOOLS_USE_TIMERS ON)
set(CPP_TOOLS_USE_PARALLEL_MANAGER ON)
```
And then include the `init-cpptools.cmake` file in your cmake to initialise `cpp_tools` : 

```cmake
include(cmake/init-cpptools)
```

This will give you access to the following targets :
 - cpp_tools::cl_parser
 - cpp_tools::colors
 - cpp_tools::timers
 - cpp_tools::parallel_manager

Now, you just have to use these targets in your cmake !

```cmake
target_link_libraries(myexe PRIVATE cpp_tools::colors cpp_tools::cl_parser)
```

## Exporting and installing

If `cpp_tools` becomes a dependency for you, and you need to forwad its 
headers in your installation process, you can do it as follow.

First you need to include `export-cpptools.cmake` in your cmake intall process.

```cmake
include(cmake/export-cpptools)
```

This will generate all the target you need as dependencies (i.e `cpp_tools::colors` etc)
in the following folder `/your/install/prefix/lib/cmake/cpp_tools/` and copy the include files
required in `/your/install/prefix/include/cpp_tools/`.

Now you need to add the following entry in your `package-nameConfig.cmake.in` file *BEFORE* you include your project targets :
```cmake
...
  include("@CMAKE_INSTALL_PREFIX@/lib/cmake/cpp_tools/cpp-tools-targets.cmake")
  include("@CMAKE_INSTALL_PREFIX@/lib/cmake/@YOUR_PROJECT_NAME@/@YOUR_PROJECT_NAME@-targets.cmake")
...
```
And that's it !

## Documentation

[cl_parser](cl_parser/README.md)
[colors](colors/README.md)
[timers](timers/README.md)
[parallel_manager](parallel_manager/README.md)


:construction: Docs of the modules can be found [here](https://compose.gitlabpages.inria.fr/oldstack/cpp_tools/) (under construction)


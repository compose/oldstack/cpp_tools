install(TARGETS ${CPP_TOOLS_TARGETS}
        EXPORT  cpp-tools-targets
        LIBRARY DESTINATION lib
        ARCHIVE DESTINATION lib
        RUNTIME DESTINATION bin
        INCLUDES DESTINATION include
        )
install(EXPORT cpp-tools-targets
        FILE cpp-tools-targets.cmake
        NAMESPACE cpp_tools::
        DESTINATION lib/cmake/cpp_tools)

foreach(target ${CPP_TOOLS_TARGETS})
    install(DIRECTORY ${CPP_TOOLS_ROOT}/${target}/include/cpp_tools/${target}
            DESTINATION ${CMAKE_INSTALL_PREFIX}/include/cpp_tools/
    )
endforeach()

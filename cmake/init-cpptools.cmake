# File for initializing cpp-tools as a submodule

if(CPP_TOOLS_USE_CL_PARSER)
    include(cmake/cl_parser)
endif()
if(CPP_TOOLS_USE_COLORS)
    include(cmake/colors)
endif()
if(CPP_TOOLS_USE_TIMERS)
    include(cmake/timers)
endif()
if(CPP_TOOLS_USE_PARALLEL_MANAGER)
    include(cmake/parallel_manager)
endif()
